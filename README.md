# Test task for KnowledgeCity

Steps for deploy:

- copy .env.example as .env (with correct credential for DB)
- run `php database/migrations.php` and `php database/seeds.php` for create tables and seeds data (or use db.sql in root folder)

Api documentation for project you can find in `knowledgecity.postman_collection.json` in root folder

- `GET` http://knowledgecity.loc/users?sort=id&order=asc&countOnPage=5&page=1
- `POST` http://knowledgecity.loc/auth (with body login=admin and password=qweqweqwe) *Bearer token is required
- `DELETE` http://knowledgecity.loc/auth *Bearer token is required