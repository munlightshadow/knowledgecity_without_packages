<?php

namespace App\Controllers;

use App\Models\User;
use App\Helpers\JwtHelper;

/**
 * Student controller
 * @author Alexander Kalksov <munlightshadow@gmail.com>
 */
class AuthController
{
    private $db;
    private $userModel;

    public function __construct($db)
    {
        $this->db = $db;
        $this->userModel = new User($db);
    }

    public function login()
    {        
        $login = (isset($_POST['login']) ? $_POST['login'] : '');
        $password = (isset($_POST['password']) ? $_POST['password'] : '');

        $user = $this->userModel->findByLogin($login);

        if (!$user) {
            header("HTTP/1.1 401 Unauthorized");            
            return json_encode(['message' => 'login or password incorrect']);
        }

        if (password_verify($password, $user['password'])) {
            $token = JwtHelper::generateToken($user['id']);
            $this->userModel->update($user['id'], ['token' => $token]);

            return json_encode(['token' => $token]);
        } else {
            header("HTTP/1.1 401 Unauthorized");            
            return json_encode(['message' => 'login or password incorrect']);
        }
    }

    public function logout()
    {
        $authUser = $this->userModel->checkToken();

        $this->userModel->update($authUser['id'], ['token' => Null]);

        return json_encode(['message' => 'Logout success']);
    }    
}