<?php

namespace App\Controllers;

use App\Models\Student;
use App\Models\User;
use App\Helpers\JwtHelper;

/**
 * Student controller
 * @author Alexander Kalksov <munlightshadow@gmail.com>
 */
class UserController
{
    private $db;
    private $userModel;
    private $studentModel;

    public function __construct($db)
    {
        $this->db = $db;
        $this->userModel = new User($db);
        $this->studentModel = new Student($db);
    }

    public function students()
    {
        $authUser = $this->userModel->checkToken();

        $sort = (isset($_GET['sort']) ? $_GET['sort'] : 'id');
        $order = (isset($_GET['order']) ? $_GET['order'] : 'asc');
        $countOnPage = (isset($_GET['countOnPage']) ? $_GET['countOnPage'] : 5);
        $page = (isset($_GET['page']) ? $_GET['page'] : 1);

        $students = $this->studentModel->getStudentsList($authUser['id'], $page, $sort, $order, $countOnPage);

        return $students;
    }
}