<?php

namespace App\Models;

class Student
{
    private $db = null;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function getStudentsList($authUserId, $page = 1, $sort = 'id', $order = 'asc', $countOnPage = 5)
    {
        $statement = "SELECT count(*) FROM students WHERE user_id = ?";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array($authUserId));
            $total_results = $statement->fetchColumn();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }   
        $total_pages = ceil($total_results/$countOnPage);


        $starting_limit = ($page - 1) * $countOnPage;
        $statement  = "SELECT * FROM students WHERE user_id = ? ORDER BY ? ? LIMIT ?,?";

        try {
            $statement = $this->db->prepare($statement);
            $statement->bindParam(1, $authUserId, \PDO::PARAM_INT);
            $statement->bindParam(2, $sort, \PDO::PARAM_STR);
            $statement->bindParam(3, $order, \PDO::PARAM_STR);
            $statement->bindParam(4, $starting_limit, \PDO::PARAM_INT);
            $statement->bindParam(5, $countOnPage, \PDO::PARAM_INT);
            $statement->execute();
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }  

        return json_encode([
            'total' => $total_pages,
            'data' => $result
        ]);
    }

    public function insert($data)
    {
        $statement = "
            INSERT INTO students 
                (username, name, surname, user_id)
            VALUES
                (:username, :name, :surname, :user_id);
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'username' => $data['username'],
                'name'  => $data['name'],
                'surname' => $data['surname'],
                'user_id' => $data['user_id']

            ));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }    
    }    
}