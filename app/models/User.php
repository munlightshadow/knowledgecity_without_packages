<?php

namespace App\Models;

use App\Helpers\JwtHelper;

class User
{
    private $db = null;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function findByLogin($username)
    {
        $statement = "
            SELECT 
                id, username, password, token
            FROM
                api_users
            WHERE username = ?;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array($username));
            $result = $statement->fetch(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }    
    }
    
    public function insert($data)
    {
        $statement = "
            INSERT INTO api_users 
                (username, password)
            VALUES
                (:username, :password);
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'username' => $data['username'],
                'password'  => password_hash($data['password'], PASSWORD_BCRYPT)
            ));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }    
    }


    public function update($id, $data)
    {     
        $setString = rtrim(
                (array_key_exists('username', $data) ? 'username = :username,' : '')  .
                (array_key_exists('password', $data) ? 'password  = :password,' : '')  .
                (array_key_exists('token', $data) ? 'token = :token,' : ''), ',');

        $statement = "
            UPDATE api_users
            SET " . $setString . " WHERE id = :id;";

        $updateKeys = ['username', 'password', 'token'];
        $updateArray = ['id' => (int) $id];

        foreach ($updateKeys as $key => $value) {
            if (array_key_exists($value, $data)) {
                $updateArray[$value] = $data[$value];
            }
        }

        if (isset($updateArray['password'])) {
            $updateArray['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
        }

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute($updateArray);
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }    
    }

    public function checkToken() 
    {
        $token = JwtHelper::getBearerToken();

        if (!$token) {
            header("HTTP/1.1 401 Unauthorized");            
            exit(json_encode(['message' => 'token not exist']));            
        }

        $statement = "
            SELECT 
                id, username, token
            FROM
                api_users
            WHERE token = ?;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array($token));
            $authUser = $statement->fetch(\PDO::FETCH_ASSOC);
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }    

        if (!$authUser) {
            header("HTTP/1.1 401 Unauthorized");            
            exit(json_encode(['message' => 'token incorrect']));
        } else {
            return $authUser;
        }
    }
}
