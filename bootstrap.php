<?php

function autoload( $class, $dir = null ) {
	$class = explode('\\', $class);
	$class = end($class);

	if ( is_null( $dir ) )
		$dir = './';

	foreach ( scandir( $dir ) as $file ) {

		if ( is_dir( $dir.$file ) && substr( $file, 0, 1 ) !== '.' )
		autoload( $class, $dir.$file.'/' );

		if ( substr( $file, 0, 2 ) !== '._' && preg_match( "/.php$/i" , $file ) ) {
			if ( str_replace( '.php', '', $file ) == $class || str_replace( '.class.php', '', $file ) == $class ) {
				include $dir . $file;
			}
		}
	}
}

spl_autoload_register("autoload");


use Core\EnvPars;
use Core\DbConnection;

$envs = new EnvPars();
$envs->load();

$dbConnection = (new DbConnection())->getConnection();