<?php

//add display error
error_reporting(E_ALL);
ini_set('display_errors', 'on');

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include __DIR__ . "/bootstrap.php";
include 'routes/route.php';

// php database/migrations.php
// php database/seeds.php