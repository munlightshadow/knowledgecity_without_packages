<?php

use Core\Route;

use App\Controllers\UserController;
use App\Controllers\AuthController;

$authController = new AuthController($dbConnection);
$userController = new UserController($dbConnection);

// Auth routs
Route::add('/auth', function() use ($authController) {return $authController->login();}, 'post');
Route::add('/auth', function() use ($authController) {return $authController->logout();}, 'delete');

// User routs
Route::add('/users', function() use ($userController) {return $userController->students();}, 'get');

// Run the router
Route::run('/');